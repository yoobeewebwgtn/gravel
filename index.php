<?php namespace Gravel;

use PDO;
use ErrorException;
use Exception;

function exception_error_handler($errno, $errstr, $errfile, $errline)
{
    if (!(error_reporting() & $errno)) {
        // This error code is not included in error_reporting
        return;
    }
    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
}
set_error_handler('\Gravel\exception_error_handler');

require "vendor/autoload.php";
require "config.inc.php";

try {
    // SESSION
    $sessionManager = new SessionManager();

    // DATABASE
    $databaseConnection = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
    // throw exceptions when we have sql errors
    $databaseConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // do not emulate preparation of statements
    $databaseConnection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

    DatabaseModel::registerDatabaseConnection($databaseConnection);

    // AUTHENTICATION
    $authenticationManager = new AuthenticationManager();

    Controller::registerSessionManager($sessionManager);
    Controller::registerAuthenticationManager($authenticationManager);

    View::registerSessionManager($sessionManager);
    View::registerAuthenticationManager($authenticationManager);

    // ROUTES
    require "routes.php";
} catch (ModelNotFoundException $e) {
    $controller = new PageController();
    $controller->error404();
} catch (Exception $e) {
    error_log($e->getMessage());
    $controller = new PageController();
    $controller->error500($e);
}
