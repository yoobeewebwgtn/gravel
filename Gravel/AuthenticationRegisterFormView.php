<?php namespace Gravel;

class AuthenticationRegisterFormView extends View
{
    public function __construct()
    {
        parent::__construct('authentication.register.form');
        $this->attributes['input'] = [];
    }

    protected function pageTitle()
    {
        echo "Sign Up";
    }

    protected function pageContents()
    {
        $this->includeTemplate($this->name);
    }
}
