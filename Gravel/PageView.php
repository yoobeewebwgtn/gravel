<?php namespace Gravel;

class PageView extends View
{
    protected function pageTitle()
    {
        echo ucwords(explode('.', $this->name)[0]);
    }

    protected function pageContents()
    {
        $this->includeTemplate($this->name);
    }
}
