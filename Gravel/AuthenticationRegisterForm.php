<?php namespace Gravel;

class AuthenticationRegisterForm extends Model
{
    public function __construct(Array $data)
    {
        $this->attributes = [
            'email'     => '',
            'password'  => '',
            'password2' => '',
        ];
        $this->attributes = array_merge($this->attributes, $data);
    }
}
