<?php namespace Gravel;

class LocationView extends View
{
    protected function pageTitle()
    {
        echo $this->attributes['location']->name;
    }

    protected function pageContents()
    {
        $this->includeTemplate($this->name);
    }
}
