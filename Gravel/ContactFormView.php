<?php namespace Gravel;

class ContactFormView extends View
{
    public function __construct()
    {
        parent::__construct('contact-form');
        $this->attributes['input'] = [];
        $this->attributes['errors'] = [];
    }

    protected function pageTitle()
    {
        echo "Contact Us";
    }

    protected function pageContents()
    {
        $this->includeTemplate($this->name);
    }
}
