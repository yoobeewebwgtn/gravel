<?php namespace Gravel;

class AuthenticationLoginForm extends Model
{
    public function __construct(Array $data)
    {
        $this->attributes = [
            'email'    => '',
        ];
        $this->attributes = array_merge($this->attributes, $data);
    }
}
