<?php namespace Gravel;

class ContactFormValidator extends Validator
{
    protected $rules =    [
        'fullname' => ['\Gravel\Validator::checkRequired', '\Gravel\Validator::checkName'],
        'email' => ['\Gravel\Validator::checkRequired', '\Gravel\Validator::checkEmail'],
        'message' => ['\Gravel\Validator::checkMessage'],
    ];
}
