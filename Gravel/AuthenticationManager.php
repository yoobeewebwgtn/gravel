<?php namespace Gravel;

class AuthenticationManager
{
    private $currentUser;

    public function __construct()
    {
        if (!isset($_SESSION['AuthenticationManager'])) {
            $this->resetSession();
        }
        if ($_SESSION['AuthenticationManager']['currentUserEmail']) {
            try {
                $this->currentUser = User::findBy('email', $_SESSION['AuthenticationManager']['currentUserEmail']);
            } catch (ModelNotFoundException $e) {
                // user is gone, reset session
                $this->resetSession();
            }
        }
    }

    protected function resetSession()
    {
        $_SESSION['AuthenticationManager'] = [
            'currentUserEmail' => null,
        ];
    }

    public function attempt($email, $password)
    {
        // get user with matching email
        try {
            $user = User::findBy('email', $email);
        } catch (ModelNotFoundException $e) {
            return false;
        }

        // compare passwords
        if ($this->comparePasswords($password, $user)) {
            $this->loginUser($user);

            return true;
        }

        return false;
    }

    public function check()
    {
        return ($this->currentUser ? true : false);
    }

    private function comparePasswords($password, User $user)
    {
        if (password_verify($password, $user->password)) {
            if (password_needs_rehash($user->password, PASSWORD_DEFAULT)) {
                $user->password = password_hash($password, PASSWORD_DEFAULT);
                $user->store();
            }

            return true;
        }

        return false;
    }

    public function loginUser(User $user)
    {
        $_SESSION['AuthenticationManager']['currentUserEmail'] = $user->email;
        $this->currentUser = $user;
    }

    public function logout()
    {
        unset($_SESSION['AuthenticationManager']);
        $this->currentUser = null;
    }

    public function user()
    {
        return $this->currentUser;
    }

    public function hasRole($role)
    {
        if ($this->isAdmin()) {
            return true;
        }

        return $this->currentUser->role === $role;
    }

    public function isAdmin()
    {
        return $this->currentUser->role === 'admin';
    }
}
