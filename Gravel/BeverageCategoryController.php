<?php namespace Gravel;

class BeverageCategoryController extends Controller
{
    public function index()
    {
        $this->authRole('admin');

        $categories = BeverageCategory::all();

        $view = new PageView('beveragecategories.index');
        $view->make(compact('categories'));
    }

    public function create()
    {
        $this->authRole('admin');

        $input = $this->session->input ?: [];
        $errors = $this->session->errors ?: [];

        // var_dump($input);
        // var_dump($errors);

        $beverage = new BeverageCategory($input);

        $view = new BeverageCategoryFormView();
        $view->make([
            'input'      => $beverage,
            'errors'     => $errors,
            'action'     => 'store',
            'verb'       => 'Create',
        ]);
    }

    public function store(Array $input)
    {
        $this->authRole('admin');

        $category = new BeverageCategory($input);

        $validator = new BeverageCategoryValidator($category->attributes);

        if (!$validator->isValid()) {
            $this->session->input = $category->attributes;
            $this->session->errors = $validator->errors;

            $this->session->setFlash('warning', "There are problems with what you submitted.");
            $this->redirect("./?page=beveragecategories.create");
        }

        $category->store();

        $this->session->setFlash('success', "Beverage Category created!");
        $this->redirect("./?page=beveragecategories.show&id=".$category->id);
    }

    public function show($id)
    {
        $this->authRole('admin');

        $category = BeverageCategory::find($id);
        $beverages = Beverage::findByCategory($id);
        $view = new BeverageCategoryView('beveragecategories.show');
        $view->make(compact('category', 'beverages'));
    }

    public function edit($id)
    {
        $this->authRole('admin');

        $input = $this->session->input ?: [];
        $errors = $this->session->errors ?: [];

        $category = BeverageCategory::find($id);

        $category->attributes = array_merge($category->attributes, $input);

        $view = new BeverageCategoryFormView();
        $view->make([
            'input'      => $category,
            'errors'     => $errors,
            'action'     => 'update',
            'verb'       => 'Update',
        ]);
    }

    public function update(Array $input)
    {
        $this->authRole('admin');

        $category = new BeverageCategory($input);

        $validator = new BeverageCategoryValidator($category->attributes);

        if (!$validator->isValid()) {
            $this->session->input = $category->attributes;
            $this->session->errors = $validator->errors;

            $this->session->setFlash('warning', "There are problems with what you submitted.");
            $this->redirect("./?page=beveragecategories.edit&id=".$category->id);
        }

        $category->store();

        $this->session->setFlash('success', "Changes saved.");
        $this->redirect("./?page=beveragecategories.show&id=".$category->id);
    }

    public function delete($id)
    {
        $this->authRole('admin');

        $category = BeverageCategory::find($id);
        $beverages = Beverage::findByCategory($id);

        $view = new PageView('beveragecategories.delete');

        $view->make(compact('category', 'beverages'));
    }

    public function destroy(Array $input)
    {
        $this->authRole('admin');

        $category = BeverageCategory::find($input['id']);

        $category->destroy();
        $this->session->setFlash('success', "Beverage Category deleted.");
        $this->redirect("./?page=beveragecategories");
    }
}
