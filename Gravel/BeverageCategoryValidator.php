<?php namespace Gravel;

class BeverageCategoryValidator extends Validator
{
    public static $db;
    public static $id = 0;

    protected $rules =  [
        'name' => ['\Gravel\Validator::checkRequired', '\Gravel\Validator::checkName'],
    ];

    public function __construct($attributes)
    {
        static::$id = (isset($attributes['id']) && $attributes['id'] > 0) ? $attributes['id'] : 0;
        parent::__construct($attributes);
    }
}
