<?php namespace Gravel;

class AuthenticationController extends Controller
{
    public function login()
    {
        $input = $this->session->input ?: [];

        $loginForm = new AuthenticationLoginForm($input);

        $view = new AuthenticationLoginFormView();
        $view->make([
            'input' => $loginForm,
        ]);
    }

    public function attempt($input)
    {
        $loginForm = new AuthenticationLoginForm($input);

        $this->auth->attempt($loginForm->email, $loginForm->password);
        if (!$this->auth->check()) {
            $this->session->input = $loginForm->attributes;

            $this->session->setFlash('danger', "Username and Password didn't match anyone we know.");
            $this->redirect("./?page=auth.login");
        }
        $this->session->setFlash('success', "Logged in!");
        $this->redirect("./");
    }

    public function logout()
    {
        $this->auth->logout();
        $this->session->setFlash('success', "Logged out.");

        $this->redirect("./");
    }

    public function create()
    {
        $input = $this->session->input ?: [];
        $errors = $this->session->errors ?: [];

        $registerForm = new AuthenticationRegisterForm($input);

        $view = new AuthenticationRegisterFormView();
        $view->make([
            'input' => $registerForm,
            'errors' => $errors,
        ]);
    }

    public function store($input)
    {
        $user = new User($input);

        $validator = new UserValidator($user->attributes);

        if (!$validator->isValid()) {
            $this->session->input = $user->attributes;
            $this->session->errors = $validator->errors;

            $this->session->setFlash('warning', "There are problems with what you submitted.");
            $this->redirect("./?page=auth.create");
        }

        $user->password = password_hash($user->password, PASSWORD_DEFAULT);

        $user->store();

        $this->session->setFlash('success', "User created! You can now log in.");
        $this->redirect("./?page=auth.login");
    }
}
