<?php namespace Gravel;

use \PDO;

class User extends DatabaseModel
{
    protected static $table = "users";

    public function __construct(Array $data)
    {
        parent::__construct();
        $this->attributes = [
            'id'          => 0,
            'email'       => '',
            'password'    => '',
            'role'        => '',
        ];
        $this->attributes = array_merge($this->attributes, $data);
    }

    public function insert()
    {
        $statement = $this->db->prepare(
            "INSERT INTO ".static::$table." (email, password) VALUES (:email, :password)"
        );

        $statement->bindValue(':email', $this->attributes['email']);
        $statement->bindValue(':password', $this->attributes['password']);

        $statement->execute();

        $this->id = $this->db->lastInsertId();
    }

    public function update()
    {
        $statement = $this->db->prepare(
            "UPDATE ".static::$table." SET email = :email, password = :password WHERE id = :id;"
        );
        $statement->bindValue(':email', $this->attributes['email']);
        $statement->bindValue(':password', $this->attributes['password']);
        $statement->bindValue(":id", $this->id, PDO::PARAM_INT);

        $statement->execute();
    }

    public static function checkIfEmailTaken($email, $id)
    {
        $db = static::$databaseConnection;

        $statement = $db->prepare("SELECT COUNT(id) FROM ".static::$table." WHERE email LIKE :email AND id != :id");
        $statement->bindValue(':email', $email);
        $statement->bindValue(':id', $id, PDO::PARAM_INT);
        $statement->execute();

        $rowCount = $statement->fetchColumn();

        return ($rowCount > 0);
    }
}
