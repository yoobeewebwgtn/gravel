<?php namespace Gravel;

class AuthenticationLoginFormView extends View
{
    public function __construct()
    {
        parent::__construct('authentication.login.form');
        $this->attributes['input'] = [];
    }

    protected function pageTitle()
    {
        echo "Sign In";
    }

    protected function pageContents()
    {
        $this->includeTemplate($this->name);
    }
}
