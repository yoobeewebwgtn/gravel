<?php namespace Gravel;

class ContactFormController extends Controller
{
    public function form()
    {
        $input = $this->session->input ?: [];
        $errors = $this->session->errors ?: [];

        $contactForm = new ContactForm($input);

        $view = new ContactFormView();
        $view->make([ 'input' => $contactForm, 'errors' => $errors ]);
    }

    public function submit(Array $input)
    {
        $contactForm = new ContactForm($input);

        $validator = new ContactFormValidator($contactForm->attributes);

        if (!$validator->isValid()) {
            $this->session->input = $contactForm->attributes;
            $this->session->errors = $validator->errors;

            $this->session->setFlash('warning', "There are problems with what you submitted.");
            $this->redirect("./?page=contact");
        }

        $contactForm->store();

        $this->redirect("./?page=contact.success");
    }

    public function success()
    {
        $view = new PageView('contact-success');
        $view->make();
    }
}
