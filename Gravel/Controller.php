<?php namespace Gravel;

abstract class Controller
{
    protected static $sessionManager;
    protected static $authenticationManager;

    public function __construct()
    {
        $this->session = static::$sessionManager;
        $this->auth    = static::$authenticationManager;
    }

    public static function registerSessionManager(SessionManager $sessionManager)
    {
        static::$sessionManager = $sessionManager;
    }

    public static function registerAuthenticationManager(AuthenticationManager $authenticationManager)
    {
        static::$authenticationManager = $authenticationManager;
    }

    public function redirect($url)
    {
        $host  = $_SERVER['HTTP_HOST'];
        $uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
        header("Location: http://".$host.$uri."/".$url);
        die();
    }

    protected function authRole($role = 'user')
    {
        if (!$this->auth->check()) {
            $this->session->setFlash('warning', 'Sorry, you need to be logged in.');
            $this->redirect('./');
        }
        if (!$this->auth->hasRole($role)) {
            $this->session->setFlash('warning', "Sorry, you don't have rights for that.");
            $this->redirect('./');
        }
    }
}
