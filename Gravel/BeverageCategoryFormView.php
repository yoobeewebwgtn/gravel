<?php namespace Gravel;

class BeverageCategoryFormView extends View
{
    public function __construct()
    {
        parent::__construct('beveragecategories.form');
        $this->attributes['input'] = [];
        $this->attributes['errors'] = [];
    }

    protected function pageTitle()
    {
        echo "Manage Beverage Category";
    }

    protected function pageContents()
    {
        $this->includeTemplate($this->name);
    }
}
