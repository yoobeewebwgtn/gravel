<?php namespace Gravel;

class ContactForm extends Model
{
    public function __construct(Array $data)
    {
        $this->attributes = [
            'fullname' => '',
            'email'       => '',
            'message'  => '',
        ];
        $this->attributes = array_merge($this->attributes, $data);
    }

    public function store()
    {
        // SEND THE MESSAGE HERE
    }
}
