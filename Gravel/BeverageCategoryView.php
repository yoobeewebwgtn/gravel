<?php namespace Gravel;

class BeverageCategoryView extends View
{
    protected function pageTitle()
    {
        echo $this->attributes['category']->name;
    }

    protected function pageContents()
    {
        $this->includeTemplate($this->name);
    }
}
