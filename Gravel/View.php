<?php namespace Gravel;

abstract class View
{
    protected $name;
    protected $masterTemplate  = "master";
    protected $flashesTemplate = "master.flashes";
    protected $template;
    protected $attributes = [];
    protected $session;

    protected static $sessionManager;
    protected static $authenticationManager;

    public function __construct($name)
    {
        $this->name = $name;

        $this->session = static::$sessionManager;
        $this->auth    = static::$authenticationManager;
    }

    public static function registerSessionManager(SessionManager $sessionManager)
    {
        static::$sessionManager = $sessionManager;
    }

    public static function registerAuthenticationManager(AuthenticationManager $authenticationManager)
    {
        static::$authenticationManager = $authenticationManager;
    }

    public function make(Array $attributes = [])
    {
        $this->attributes = array_merge($this->attributes, $attributes);
        $this->render();
    }

    protected function templatePath($name)
    {
        return $this->template = PATH_TEMPLATES.$name.".tpl.php";
    }

    private function render()
    {
        $this->includeTemplate($this->masterTemplate);
    }

    protected function includeTemplate($name)
    {
        extract($this->attributes);
        include $this->templatePath($name);
    }

    abstract protected function pageTitle();

    abstract protected function pageContents();

    protected function pageFlashes()
    {
        $this->includeTemplate($this->flashesTemplate);
    }

    protected function echoWhenViewName($name, $str)
    {
        $primaryName = explode(".", $this->name);
        $primaryName = $primaryName[0];
        if ($primaryName === $name) {
            echo $str;
        }
    }

    protected function echoIfError($fieldName, $str)
    {
        if (!isset($this->attributes['errors'][$fieldName])) {
            return false;
        }
        if ($this->attributes['errors'][$fieldName] !== "") {
            echo $str;
        }
    }

    protected function showError($fieldName)
    {
        if (!isset($this->attributes['errors'][$fieldName])) {
            return false;
        }
        if ($this->attributes['errors'][$fieldName] !== "") {
            echo '<span class="glyphicon glyphicon-remove form-control-feedback"></span>';
            echo '<span class="help-block">' . $this->attributes['errors'][$fieldName] . '</span>';
        }
    }

    public function ee($str)
    {
        echo htmlentities($str, null, 'UTF-8');
    }

    public function plural($count, $single, $plural)
    {
        if ((int) $count === 1) {
            echo $single;
        } else {
            echo $plural;
        }
    }
}
