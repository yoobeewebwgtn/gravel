<?php namespace Gravel;

class PageController extends Controller
{
    public function home()
    {
        $view = new PageView('home');
        $view->make();
    }
    public function about()
    {
        $view = new PageView('about');
        $view->make();
    }
    public function error404()
    {
        $view = new PageView('error.404');
        $view->make();
    }
    public function error500($exception)
    {
        $view = new PageView('error.500');
        $view->make(compact('exception'));
    }
}
