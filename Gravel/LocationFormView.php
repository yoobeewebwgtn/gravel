<?php namespace Gravel;

class LocationFormView extends View
{
    public function __construct()
    {
        parent::__construct('locations.form');
        $this->attributes['input'] = [];
        $this->attributes['errors'] = [];
    }

    protected function pageTitle()
    {
        echo "Manage Location";
    }

    protected function pageContents()
    {
        $this->includeTemplate($this->name);
    }
}
