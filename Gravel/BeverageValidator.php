<?php namespace Gravel;

class BeverageValidator extends Validator
{
    public static $id = 0;

    protected $rules =  [
        'name' => ['\Gravel\Validator::checkRequired', '\Gravel\Validator::checkName'],
        'category_id' => ['\Gravel\Validator::checkRequired', 'static::checkCategory'],
        'description' => ['\Gravel\Validator::checkMessage'],
    ];

    public function __construct($attributes)
    {
        static::$id = (isset($attributes['id']) && $attributes['id'] > 0) ? $attributes['id'] : 0;
        parent::__construct($attributes);
    }

    public static function checkCategory($value)
    {
        try {
            $category = BeverageCategory::find($value);

            return true;
        } catch (ModelNotFoundException $e) {
            return "Category does not exist, pick a valid category.";
        }
    }
}
