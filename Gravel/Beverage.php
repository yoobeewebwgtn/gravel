<?php namespace Gravel;

use PDO;

class Beverage extends DatabaseModel
{
    protected static $table = "beverages";

    public function __construct(Array $data)
    {
        parent::__construct();
        $this->attributes = [
            'id'          => 0,
            'name'        => '',
            'description' => '',
            'category_id' => 0,
        ];
        $this->attributes = array_merge($this->attributes, $data);
    }

    public function category()
    {
        return BeverageCategory::find($this->category_id);
    }

    public function insert()
    {
        $statement = $this->db->prepare(
            "INSERT INTO ".static::$table." (name, category_id, description) VALUES (:name, :category_id, :description)"
        );

        $statement->bindValue(':name', $this->attributes['name']);
        $statement->bindValue(':category_id', $this->attributes['category_id'], PDO::PARAM_INT);
        $statement->bindValue(':description', $this->attributes['description']);

        $statement->execute();

        $this->id = $this->db->lastInsertId();
    }

    public function update()
    {
        $statement = $this->db->prepare(
            "UPDATE ".static::$table." SET name = :name, category_id = :category_id, description = :description WHERE id = :id;"
        );
        $statement->bindValue(':name', $this->attributes['name']);
        $statement->bindValue(':category_id', $this->attributes['category_id'], PDO::PARAM_INT);
        $statement->bindValue(':description', $this->attributes['description']);
        $statement->bindValue(":id", $this->id, PDO::PARAM_INT);

        $statement->execute();
    }

    public static function findByCategory($category_id)
    {
        $db = static::$databaseConnection;

        $collection = array();

        // get all of the things from the database.
        $statement = $db->prepare("SELECT * FROM ".static::$table." WHERE category_id = :category_id");

        $statement->bindValue(':category_id', $category_id);
        $statement->execute();

        // get the products record by record
        while ($record = $statement->fetch(PDO::FETCH_ASSOC)) {
            $obj = new static($record);
            array_push($collection, $obj);
        }

        return $collection;
    }
}
