<?php namespace Gravel;

use PDO;

class Location extends DatabaseModel
{
    protected static $table = "locations";

    public function __construct(Array $data)
    {
        parent::__construct();
        $this->attributes = [
            'id'          => 0,
            'name'        => '',
            'slug'        => '',
            'city'          => '',
            'description' => '',
        ];
        $this->attributes = array_merge($this->attributes, $data);
    }

    public function insert()
    {
        $statement = $this->db->prepare(
            "INSERT INTO ".static::$table." (name, slug, city, description) VALUES (:name, :slug, :city, :description)"
        );

        $statement->bindValue(':name', $this->attributes['name']);
        $statement->bindValue(':slug', $this->attributes['slug']);
        $statement->bindValue(':city', $this->attributes['city']);
        $statement->bindValue(':description', $this->attributes['description']);

        $statement->execute();

        $this->id = $this->db->lastInsertId();
    }

    public function update()
    {
        $statement = $this->db->prepare(
            "UPDATE ".static::$table." SET name = :name, slug = :slug, city = :city, description = :description WHERE id = :id;"
        );
        $statement->bindValue(':name', $this->attributes['name']);
        $statement->bindValue(':slug', $this->attributes['slug']);
        $statement->bindValue(':city', $this->attributes['city']);
        $statement->bindValue(':description', $this->attributes['description']);
        $statement->bindValue(":id", $this->id, PDO::PARAM_INT);

        $statement->execute();
    }

    public static function checkIfSlugTaken($slug, $id)
    {
        $db = static::$databaseConnection;

        $statement = $db->prepare("SELECT COUNT(id) FROM ".static::$table." WHERE slug LIKE :slug AND id != :id");
        $statement->bindValue(':slug', $slug);
        $statement->bindValue(':id', $id, PDO::PARAM_INT);
        $statement->execute();

        $rowCount = $statement->fetchColumn();

        return ($rowCount > 0);
    }
}
