<?php namespace Gravel;

use \Exception;

abstract class Validator
{
    public $errors = [];
    public $attributes = [];

    protected $isValid = false;

    public function __construct(Array $attributes)
    {
        $this->attributes = $attributes;
        $this->validate();
    }

    public function validate()
    {
        $this->isValid = true;
        // all fields are required.
        // if field is blank, record error.
        $this->errors = array();
        foreach ($this->rules as $fieldName => $validationMethods) {
            $this->errors[$fieldName] = "";

            foreach ($validationMethods as $method) {
                if (!is_callable($method)) {
                    throw new Exception("found uncallable validation method $method on $fieldName");
                }
                $message = call_user_func($method, $this->attributes[$fieldName]);
                if ($message !== true) {
                    $this->errors[$fieldName] = $message;
                    $this->isValid = false;
                }
            } // end validationMethods foreach
        } // end fieldName foreach
    }

    public function isValid()
    {
        return $this->isValid;
    }

    public static function checkRequired($str)
    {
        if (!isset($str) || $str === "") {
            return "This field is required.";
        }

        return true;
    }

    public static function checkName($str)
    {
        if (strlen($str) == 1) {
            return "Must be at least 2 characters long.";
        }

        return true;
    }

    public static function checkMessage($str)
    {
        if (strlen($str) !== 0 && strlen($str) < 5) {
            return "Must be at least 5 characters long.";
        }

        return true;
    }

    public static function checkEmail($str)
    {
        if (!filter_var($str, FILTER_VALIDATE_EMAIL)) {
            return "Enter a valid email address.";
        }

        return true;
    }

    public static function checkNumeric($str)
    {
        if (!filter_var($str, FILTER_VALIDATE_FLOAT)) {
            return "Must be a valid number with decimal places.";
        }

        return true;
    }

    public static function checkPassword($str)
    {
        if (strlen($str) < 6) {
            return "Must be at least 6 characters long.";
        }
        // SOMEDAY: Add extra password restrictions,
        // such as capitals, numbers, symbols, oh my!

        return true;
    }
}
