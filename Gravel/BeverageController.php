<?php namespace Gravel;

class BeverageController extends Controller
{
    public function index()
    {
        $categories = BeverageCategory::all();

        $view = new PageView('beverages.index');
        $view->make(compact('categories'));
    }

    public function create()
    {
        $this->authRole('admin');

        $input = $this->session->input ?: [];
        $errors = $this->session->errors ?: [];

        // var_dump($input);
        // var_dump($errors);

        $beverage = new Beverage($input);
        $categories = BeverageCategory::all();

        $view = new BeverageFormView();
        $view->make([
            'input'      => $beverage,
            'errors'     => $errors,
            'categories' => $categories,
            'action'     => 'store',
            'verb'       => 'Create',
        ]);
    }

    public function store(Array $input)
    {
        $this->authRole('admin');

        $beverage = new Beverage($input);

        $validator = new BeverageValidator($beverage->attributes);

        if (!$validator->isValid()) {
            $this->session->input = $beverage->attributes;
            $this->session->errors = $validator->errors;

            $this->session->setFlash('warning', "There are problems with what you submitted.");
            $this->redirect("./?page=beverages.create");
        }

        $beverage->store();

        $this->session->setFlash('success', "Beverage created!");
        $this->redirect("./?page=beverages.show&id=".$beverage->id);
    }

    public function show($id)
    {
        $beverage = Beverage::find($id);

        $view = new BeverageView('beverages.show');
        $view->make(compact('beverage'));
    }

    public function edit($id)
    {
        $this->authRole('admin');

        $input = $this->session->input ?: [];
        $errors = $this->session->errors ?: [];

        $beverage = Beverage::find($id);
        $categories = BeverageCategory::all();

        $beverage->attributes = array_merge($beverage->attributes, $input);

        $view = new BeverageFormView();
        $view->make([
            'input'      => $beverage,
            'errors'     => $errors,
            'categories' => $categories,
            'action'     => 'update',
            'verb'       => 'Update',
        ]);
    }

    public function update(Array $input)
    {
        $this->authRole('admin');

        $beverage = new Beverage($input);

        $validator = new BeverageValidator($beverage->attributes);

        if (!$validator->isValid()) {
            $this->session->input = $beverage->attributes;
            $this->session->errors = $validator->errors;

            $this->session->setFlash('warning', "There are problems with what you submitted.");
            $this->redirect("./?page=beverages.edit&id=".$beverage->id);
        }

        $beverage->store();

        $this->session->setFlash('success', "Changes saved.");
        $this->redirect("./?page=beverages.show&id=".$beverage->id);
    }

    public function delete($id)
    {
        $this->authRole('admin');

        $beverage = Beverage::find($id);

        $view = new PageView('beverages.delete');

        $view->make(compact('beverage'));
    }

    public function destroy(Array $input)
    {
        $this->authRole('admin');

        $beverage = Beverage::find($input['id']);

        $beverage->destroy();
        $this->session->setFlash('success', "Beverage deleted.");
        $this->redirect("./?page=beverages");
    }
}
