<?php namespace Gravel;

class SessionManager
{
    private $old = [];

    const SESSION_NAMESPACE = "SessionManager";

    static public $flashes = ['success', 'info', 'warning', 'danger'];

    public function __construct()
    {
        session_start();
        session_regenerate_id(true);
        $this->moveCurrentToOld();
    }

    public function __set($key, $value)
    {
        $_SESSION[self::SESSION_NAMESPACE][$key] = $value;
    }

    public function __get($key)
    {
        if (isset($_SESSION[self::SESSION_NAMESPACE][$key])) {
            return $_SESSION[self::SESSION_NAMESPACE][$key];
        }
        if (isset($this->old[$key])) {
            return $this->old[$key];
        }

        return null;
    }

    public function setFlash($flash, $value)
    {
        if (!in_array($flash, static::$flashes)) {
            throw new UnexpectedValueException("Argument 1 must be one of: ".implode(", ", static::$flashes));
        }
        $_SESSION[self::SESSION_NAMESPACE]['flash'][$flash] = $value;
    }

    private function moveCurrentToOld()
    {
        if (isset($_SESSION[self::SESSION_NAMESPACE])) {
            $this->old = $_SESSION[self::SESSION_NAMESPACE];
        }
        $_SESSION[self::SESSION_NAMESPACE] = [];
    }
}
