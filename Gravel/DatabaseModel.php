<?php namespace Gravel;

use PDO;

abstract class DatabaseModel extends Model
{
    protected static $table = "";

    protected static $databaseConnection;

    protected $db;

    public function __construct()
    {
        $this->db = static::$databaseConnection;
    }

    public static function registerDatabaseConnection(PDO $databaseConnection)
    {
        static::$databaseConnection = $databaseConnection;
    }

    public function store()
    {
        if ($this->id > 0) {
            $this->update();
        } else {
            $this->insert();
        }
    }

    abstract public function insert();

    abstract public function update();

    public function destroy()
    {
        $statement = $this->db->prepare(
            "DELETE FROM ".static::$table." WHERE id = :id;"
        );
        $statement->bindValue(":id", $this->id, PDO::PARAM_INT);
        $statement->execute();
    }

    public static function find($id)
    {
        $db = static::$databaseConnection;

        $statement = $db->prepare("SELECT * FROM ".static::$table." WHERE id = :id;");
        $statement->bindValue(":id", $id, PDO::PARAM_INT);

        $statement->execute();

        // get the first record
        $record = $statement->fetch(PDO::FETCH_ASSOC);
        if (!$record) {
            throw new ModelNotFoundException();
        }
        $obj = new static($record);

        return $obj;
    }

    public static function findBy($column, $value)
    {
        $db = static::$databaseConnection;

        $statement = $db->prepare("SELECT * FROM ".static::$table." WHERE ".$column." = :value ;");
        $statement->bindValue(":value", $value);

        $statement->execute();

        // get the first record
        $record = $statement->fetch(PDO::FETCH_ASSOC);
        if (!$record) {
            throw new ModelNotFoundException();
        }
        $obj = new static($record);

        return $obj;
    }

    public static function all()
    {
        $db = static::$databaseConnection;

        $collection = array();

        $statement = $db->prepare("SELECT * FROM ".static::$table);
        $statement->execute();

        // get each record
        while ($record = $statement->fetch(PDO::FETCH_ASSOC)) {
            $obj = new static($record);
            array_push($collection, $obj);
        }

        return $collection;
    }
}
