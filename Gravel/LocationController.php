<?php namespace Gravel;

class LocationController extends Controller
{
    public function index()
    {
        $locations = Location::all();

        $view = new PageView('locations.index');
        $view->make(compact('locations'));
    }

    public function create()
    {
        $this->authRole('admin');
        $input = $this->session->input ?: [];
        $errors = $this->session->errors ?: [];

        // var_dump($input);
        // var_dump($errors);

        $location = new Location($input);

        $view = new LocationFormView();
        $view->make([
            'input'  => $location,
            'errors' => $errors,
            'action' => 'store',
            'verb'   => 'Create',
        ]);
    }

    public function store(Array $input)
    {
        $this->authRole('admin');

        $location = new Location($input);

        $validator = new LocationValidator($location->attributes);

        if (!$validator->isValid()) {
            $this->session->input = $location->attributes;
            $this->session->errors = $validator->errors;

            $this->session->setFlash('warning', "There are problems with what you submitted.");
            $this->redirect("./?page=locations.create");
        }

        $location->store();

        $this->session->setFlash('success', "Location created!");
        $this->redirect("./?page=locations.show&id=".$location->id);
    }

    public function show($id)
    {
        $location = Location::find($id);

        $view = new LocationView('locations.show');
        $view->make(compact('location'));
    }

    public function edit($id)
    {
        $this->authRole('admin');

        $input = $this->session->input ?: [];
        $errors = $this->session->errors ?: [];

        $location = Location::find($id);

        $location->attributes = array_merge($location->attributes, $input);

        $view = new LocationFormView();
        $view->make([
            'input'  => $location,
            'errors' => $errors,
            'action' => 'update',
            'verb'   => 'Update',
        ]);
    }

    public function update(Array $input)
    {
        $this->authRole('admin');

        $location = new Location($input);

        $validator = new LocationValidator($location->attributes);

        if (!$validator->isValid()) {
            $this->session->input = $location->attributes;
            $this->session->errors = $validator->errors;

            $this->session->setFlash('warning', "There are problems with what you submitted.");
            $this->redirect("./?page=locations.edit&id=".$location->id);
        }

        $location->store();

        $this->session->setFlash('success', "Changes saved.");
        $this->redirect("./?page=locations.show&id=".$location->id);
    }

    public function delete($id)
    {
        $this->authRole('admin');

        $location = Location::find($id);

        $view = new PageView('locations.delete');

        $view->make(compact('location'));
    }

    public function destroy(Array $input)
    {
        $this->authRole('admin');

        $location = Location::find($input['id']);

        $location->destroy();
        $this->session->setFlash('success', "Location deleted.");
        $this->redirect("./?page=locations");
    }
}
