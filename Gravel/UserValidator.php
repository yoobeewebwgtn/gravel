<?php namespace Gravel;



class UserValidator extends Validator
{
    public static $db;
    public static $id = '';
    public static $password = '';

    protected $rules =  [
        'email' => ['\Gravel\Validator::checkRequired', '\Gravel\Validator::checkEmail', 'static::checkEmailNotTaken'],
        'password' => ['\Gravel\Validator::checkRequired', '\Gravel\Validator::checkPassword'],
        'password2' => ['\Gravel\Validator::checkMessage'],
    ];

    public function __construct($attributes)
    {
        static::$id = (isset($attributes['id']) && $attributes['id'] > 0) ? $attributes['id'] : 0;
        static::$password = (isset($attributes['password']) && $attributes['password'] > 0) ? $attributes['password'] : 0;
        parent::__construct($attributes);
    }

    public static function checkEmailNotTaken($email)
    {
        if (User::checkIfEmailTaken($email, static::$id)) {
            return "Email address already in use, choose another.";
        }

        return true;
    }

    public static function checkPasswordsMatch($password2)
    {
        if ($password2 !== static::$password) {
            return "You must accurately enter the password twice.";
        }

        return true;
    }
}
