<?php namespace Gravel;

use PDO;

class BeverageCategory extends DatabaseModel
{
    protected static $table = "beverage_categories";

    public function __construct(Array $data)
    {
        parent::__construct();
        $this->attributes = [
            'id'          => 0,
            'name'        => '',
        ];
        $this->attributes = array_merge($this->attributes, $data);
    }

    public function beverages()
    {
        return Beverage::findByCategory($this->id);
    }

    public function insert()
    {
        $statement = $this->db->prepare(
            "INSERT INTO ".static::$table." (name) VALUES (:name)"
        );

        $statement->bindValue(':name', $this->attributes['name']);

        $statement->execute();

        $this->id = $this->db->lastInsertId();
    }

    public function update()
    {
        $statement = $this->db->prepare(
            "UPDATE ".static::$table." SET name = :name WHERE id = :id;"
        );
        $statement->bindValue(':name', $this->attributes['name']);
        $statement->bindValue(":id", $this->id, PDO::PARAM_INT);

        $statement->execute();
    }
}
