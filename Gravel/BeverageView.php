<?php namespace Gravel;

class BeverageView extends View
{
    protected function pageTitle()
    {
        echo $this->attributes['beverage']->name;
    }

    protected function pageContents()
    {
        $this->includeTemplate($this->name);
    }
}
