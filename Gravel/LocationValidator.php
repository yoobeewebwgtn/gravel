<?php namespace Gravel;

class LocationValidator extends Validator
{
    public static $db;
    public static $id = 0;

    protected $rules =  [
        'name' => ['\Gravel\Validator::checkRequired', '\Gravel\Validator::checkName'],
        'slug' => ['\Gravel\Validator::checkRequired', 'static::slugNotTaken'],
        'city' => ['\Gravel\Validator::checkRequired', '\Gravel\Validator::checkName'],
        'description' => ['\Gravel\Validator::checkMessage'],
    ];

    public function __construct($attributes)
    {
        static::$id = (isset($attributes['id']) && $attributes['id'] > 0) ? $attributes['id'] : 0;
        parent::__construct($attributes);
    }

    public static function slugNotTaken($value)
    {
        if (Location::checkIfSlugTaken($value, static::$id)) {
            return "Slug already in use, choose another.";
        }

        return true;
    }
}
