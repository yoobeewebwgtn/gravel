<?php namespace Gravel;

class BeverageFormView extends View
{
    public function __construct()
    {
        parent::__construct('beverages.form');
        $this->attributes['input'] = [];
        $this->attributes['errors'] = [];
    }

    protected function pageTitle()
    {
        echo "Manage Beverage";
    }

    protected function pageContents()
    {
        $this->includeTemplate($this->name);
    }
}
