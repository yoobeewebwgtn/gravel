<?php

if ($_SERVER['HTTP_HOST'] === "localhost") {
    define("HOST_TYPE", "dev");
} else {
    define("HOST_TYPE", "live");
}

define("PATH_TEMPLATES", "./templates/"); // trailing slash

error_reporting(E_ALL);

switch (HOST_TYPE) {
    case "dev":
        ini_set('display_errors', true);
        ini_set("log_errors", 1);
        ini_set("error_log", getcwd()."/php-error.log");
        define("DB_DSN", 'mysql:host=localhost;dbname=gravel;charset=utf8');
        define("DB_USERNAME", 'root');
        define("DB_PASSWORD", '');
        break;
    case "live":
        // no break
    default:
        ini_set('display_errors', false);
        ini_set("log_errors", 1);
        ini_set("error_log", getcwd()."/php-error.log");
        define("DB_DSN", 'mysql:host=localhost;dbname=brettt_gravel;charset=utf8');
        define("DB_USERNAME", 'brettt_gravel');
        define("DB_PASSWORD", "banana");
}
