<?php namespace Gravel;
    foreach (SessionManager::$flashes as $_flash):
        if (isset($this->session->flash[$_flash])):
?>
<div class="container">
    <div class="alert alert-<?= $_flash ?>" role="alert">
        <?= $this->session->flash[$_flash] ?>
    </div>
</div>
<?php endif; endforeach; ?>
