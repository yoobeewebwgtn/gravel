<div class="container">
  <div class="row">
    <div class="col-md-12">
      <h1><?= $beverage->name ?></h1>
      <h2><?= $beverage->category()->name ?></h2>
      <p><?= $beverage->description ?></p>
      <a class="btn btn-default" href="./?page=beverages.edit&amp;id=<?= $beverage->id ?>">
        <span class="glyphicon glyphicon-pencil"></span> Edit
      </a>
    </div>
  </div>
</div>
