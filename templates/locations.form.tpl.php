<div class="container">
  <div class="row">
    <div class="col-md-12">
      <h1><?= $verb ?> New Location</h1>
      <form method="POST" action="./?page=locations.<?= $action ?>">

        <input id="id" type="hidden" name="id" value="<?= $input->id ?>">
        <div class="form-group <?php $this->echoIfError('name', "has-error has-feedback"); ?>">
          <label for="name">Location Name</label>
          <input id="name" name="name" class="form-control"
                  value="<?php $this->ee($input->name); ?>"
                  placeholder="e.g. Somewhereville"
          />
          <?php $this->showError('name'); ?>
        </div>

        <div class="form-group <?php $this->echoIfError('slug', "has-error has-feedback"); ?>">
          <label for="slug">Slug</label>
          <input id="slug" name="slug" class="form-control"
                  value="<?php $this->ee($input->slug); ?>"
                  placeholder="e.g. somewheresville"
          />
          <?php $this->showError('slug'); ?>
        </div>

        <div class="form-group <?php $this->echoIfError('city', "has-error has-feedback"); ?>">
          <label for="city">City</label>
          <input id="city" name="city" class="form-control"
                  value="<?php $this->ee($input->city); ?>"
                  placeholder="e.g. Somewhereville"
          />
          <?php $this->showError('city'); ?>
        </div>

        <div class="form-group <?php $this->echoIfError('description', "has-error has-feedback"); ?>">
          <label for="description">Description</label>
          <textarea id="description" name="description" class="form-control"><?php $this->ee($input->description); ?></textarea>
          <?php $this->showError('description'); ?>
        </div>

        <div class="form-group">
          <button class="btn btn-primary">
            <span class="glyphicon glyphicon-ok"></span>
            <?= $verb ?>
          </button>

          <?php if ($action === "update"): ?>
            <a href="./?page=locations.delete&amp;id=<?= $input->id ?>" class="btn btn-link" id="delete">
              <span class="glyphicon glyphicon-trash"></span>
              Delete
            </a>
          <?php endif; ?>
        </div>

      </form>
    </div>
  </div>
</div>
