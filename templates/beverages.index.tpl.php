<div class="container">
  <div class="row">
    <div class="col-md-12">
      <h1>Beverages</h1>
        <?php foreach ($categories as $category): ?>
          <h2><?= $category->name ?></h2>
          <ul>
            <?php foreach ($category->beverages() as $beverage): ?>
              <li><a href="./?page=beverages.show&amp;id=<?= $beverage->id ?>"><?= $beverage->name ?></a></li>
            <?php endforeach; ?>
          </ul>
        <?php endforeach; ?>
      <a class="btn btn-default" href="./?page=beverages.create"><span class="glyphicon glyphicon-plus"></span> Add</a>
    </div>
  </div>
</div>
