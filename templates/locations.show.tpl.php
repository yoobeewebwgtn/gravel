<div class="container">
  <div class="row">
    <div class="col-md-12">
      <h1><?= $location->name ?></h1>
      <h2><?= $location->city ?></h2>
      <p><?= $location->description ?></p>
  	  <a class="btn btn-default" href="./?page=locations.edit&amp;id=<?= $location->id ?>">
  	  	<span class="glyphicon glyphicon-pencil"></span> Edit
  	  </a>
    </div>
  </div>
</div>
