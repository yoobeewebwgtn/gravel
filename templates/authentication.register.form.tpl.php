<div class="container">
  <div class="row">
    <div class="col-md-12">
      <h1>Sign Up</h1>
      <form method="POST" action="./?page=auth.store" novalidate>

        <div class="form-group <?php $this->echoIfError('email', "has-error has-feedback"); ?>">
          <label for="email">Email</label>
          <input id="email" name="email" class="form-control" type="email"
                  value="<?php $this->ee($input->email); ?>"
                  placeholder="example@example.com"
          />
          <?php $this->showError('email'); ?>
        </div>

        <div class="form-group <?php $this->echoIfError('password', "has-error has-feedback"); ?>">
          <label for="password">Password</label>
          <input id="password" name="password" class="form-control" type="password" />
          <?php $this->showError('password'); ?>
        </div>

        <div class="form-group <?php $this->echoIfError('password2', "has-error has-feedback"); ?>">
          <label for="password2">Confirm</label>
          <input id="password2" name="password2" class="form-control" type="password" />
          <?php $this->showError('password2'); ?>
        </div>


        <div class="form-group">
          <button class="btn btn-primary">
            <span class="glyphicon glyphicon-log-in"></span>
            Sign Up
          </button>

        </div>

      </form>
    </div>
  </div>
</div>
