<div class="jumbotron">
  <div class="container">
    <h1>500 Internal Server Error</h1>
    <p>Whoops, we stuffed up. Try again later.</p>

    <?php if (HOST_TYPE != 'live'): ?>
        <div class="well">
            <h2>Error: <?= get_class($exception) ?></h2>
            <p>File: <?= $exception->getFile() ?> Line <?= $exception->getLine() ?></p>
            <h3><?= $exception->getMessage() ?></h3>

            <?php foreach ($exception->getTrace() as $level => $stack): ?>
                <p><pre><strong>Stack Level <?= $level ?>:</strong><br><?php print_r($stack); ?></pre></p>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>

  </div>
</div>
