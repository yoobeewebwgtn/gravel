<div class="container">
  <div class="row">
    <div class="col-md-12">
      <h1><?= $category->name ?></h1>
      <p>Deleting this category will also delete these beverages:</p>
      <ul>
        <?php foreach ($beverages as $beverage): ?>
          <li><a href="./?page=beverages.show&amp;id=<?= $beverage->id ?>"><?= $beverage->name ?></a></li>
        <?php endforeach; ?>
      </ul>

      <form method="POST" action="./?page=beveragecategories.destroy">
        <input type="hidden" name="id" value="<?= $category->id ?>">
        <p>Really delete "<?= $category->name ?>"
          <?php if (count($beverages) > 0): ?>
            and <?= count($beverages) ?> <?= $this->plural(count($beverages), "beverage", "beverages") ?> ?</p>
          <?php endif; ?>
        <button class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Delete</button>
        <a class="btn btn-default" href="./?page=beveragecategories.show&amp;id=<?= $category->id ?>">Keep</a>
      </form>

    </div>
  </div>
</div>
