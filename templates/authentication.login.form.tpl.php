<div class="container">
  <div class="row">
    <div class="col-md-12">
      <h1>Log in</h1>
      <form method="POST" action="./?page=auth.attempt" novalidate>

        <div class="form-group">
          <label for="email">Email</label>
          <input id="email" name="email" class="form-control" type="email"
                  value="<?php $this->ee($input->email); ?>"
                  placeholder="example@example.com"
          />
        </div>

        <div class="form-group">
          <label for="password">Password</label>
          <input id="password" name="password" class="form-control" type="password" />
        </div>


        <div class="form-group">
          <button class="btn btn-primary">
            <span class="glyphicon glyphicon-log-in"></span>
            Log in
          </button>

        </div>

      </form>
    </div>
  </div>
</div>
