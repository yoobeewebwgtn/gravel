<div class="container">
  <div class="row">
    <div class="col-md-12">
      <h1>Beverage Categories</h1>
        <?php foreach ($categories as $category): ?>
          <h2><a href="./?page=beveragecategories.show&amp;id=<?= $category->id ?>"><?= $category->name ?></a></h2>
          <p>
            <?php $beverages = $category->beverages(); ?>
            <?= count($beverages) ?> <?= $this->plural(count($beverages), "beverage", "beverages") ?>
          </p>
        <?php endforeach; ?>
      <a class="btn btn-default" href="./?page=beveragecategories.create"><span class="glyphicon glyphicon-plus"></span> Add Category</a>
    </div>
  </div>
</div>
