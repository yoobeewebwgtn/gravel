<div class="container">
  <div class="row">
    <div class="col-md-12">
      <h1><?= $beverage->name ?></h1>
      <h2><?= $beverage->category()->name ?></h2>
      <p><?= $beverage->description ?></p>

      <form method="POST" action="./?page=beverages.destroy">
        <input type="hidden" name="id" value="<?= $beverage->id ?>">
        <p>Really delete "<?= $beverage->name ?>" ?</p>
        <button class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Delete</button>
        <a class="btn btn-default" href="./?page=beverages.show&amp;id=<?= $beverage->id ?>">Keep</a>
      </form>
    </div>
  </div>
</div>
