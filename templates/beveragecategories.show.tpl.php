<div class="container">
  <div class="row">
    <div class="col-md-12">
      <h1><?= $category->name ?></h1>
      <ul>
        <?php foreach ($beverages as $beverage): ?>
          <li><?= $beverage->name ?></li>
        <?php endforeach; ?>
      </ul>
      <a class="btn btn-default" href="./?page=beveragecategories.edit&amp;id=<?= $category->id ?>">
        <span class="glyphicon glyphicon-pencil"></span> Edit Category
      </a>
    </div>
  </div>
</div>
