<div class="container">
  <div class="row">
    <div class="col-md-12">
      <h1><?= $location->name ?></h1>
      <h2><?= $location->city ?></h2>
      <p><?= $location->description ?></p>
      <form method="POST" action="./?page=locations.destroy">
        <input type="hidden" name="id" value="<?= $location->id ?>">
        <p>Really delete "<?= $location->name ?>" ?</p>
        <button class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Delete</button>
        <a class="btn btn-default" href="./?page=locations.show&amp;id=<?= $location->id ?>">Keep</a>
      </form>
    </div>
  </div>
</div>
