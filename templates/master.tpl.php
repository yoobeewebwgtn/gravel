<!DOCTYPE html>
<html class="no-js">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Gravel :: <?= $this->pageTitle(); ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <style>
      body {
        padding-top: 50px;
        padding-bottom: 20px;
      }
    </style>
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="css/main.css">

    <script src="js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
  </head>
  <body>
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="./">Gravel</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li <?php $this->echoWhenViewName("home", 'class="active"'); ?>><a href="./">Home</a></li>
            <li <?php $this->echoWhenViewName("locations", 'class="active"'); ?>><a href="./?page=locations">Locations</a></li>
            <li <?php $this->echoWhenViewName("beverages", 'class="active"'); ?>><a href="./?page=beverages">Beverages</a></li>
            <li <?php $this->echoWhenViewName("about", 'class="active"'); ?>><a href="./?page=about">About</a></li>
            <li <?php $this->echoWhenViewName("contact-form", 'class="active"'); ?>><a href="./?page=contact">Contact</a></li>
          </ul>
          <?php if ($this->auth): ?>
            <?php if (!$this->auth->check()): ?>
              <div class="nav navbar-right navbar-form">
                <a class="btn btn-success" href="./?page=auth.login"><span class="glyphicon glyphicon-log-in"></span> Log in</a>
                <a class="btn btn-primary" href="./?page=auth.create"><span class="glyphicon glyphicon-pencil"></span> Sign up</a>
              </div>
            <?php else: ?>
              <div class="nav navbar-right navbar-form">
                <a class="btn btn-danger" href="./?page=auth.logout" title="Sign out"><span class="glyphicon glyphicon-log-out"></span> <?= $this->auth->user()->email ?></a>
              </div>
            <?php endif; ?>
          <?php endif; ?>
        </div><!--/.navbar-collapse -->
      </div>
    </div>

    <?= $this->pageFlashes(); ?>

    <?= $this->pageContents(); ?>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.1.min.js"><\/script>')</script>

    <script src="js/vendor/bootstrap.min.js"></script>

    <script src="js/main.js"></script>
  </body>
</html>
