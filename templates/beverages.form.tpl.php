<div class="container">
  <div class="row">
    <div class="col-md-12">
      <h1><?= $verb ?> New Beverage</h1>
      <form method="POST" action="./?page=beverages.<?= $action ?>">

        <input id="id" type="hidden" name="id" value="<?= $input->id ?>">
        <div class="form-group <?php $this->echoIfError('name', "has-error has-feedback"); ?>">
          <label for="name">Beverage Name</label>
          <input id="name" name="name" class="form-control"
                  value="<?php $this->ee($input->name); ?>"
                  placeholder="e.g. Somewhereville"
          />
          <?php $this->showError('name'); ?>
        </div>

        <div class="form-group <?php $this->echoIfError('category_id', "has-error has-feedback"); ?>">
          <label for="category_id">Beverage Category</label>
          <select class="form-control" id="category_id" name="category_id">
            <?php foreach ($categories as $category): ?>
              <option value="<?= $category->id ?>" <?php echo($input->category_id === $category->id) ? "selected" : "" ?>>
                <?= $category->name ?>
              </option>
            <?php endforeach; ?>
          </select>
          <?php $this->showError('category_id'); ?>
        </div>

        <div class="form-group <?php $this->echoIfError('description', "has-error has-feedback"); ?>">
          <label for="description">Description</label>
          <textarea id="description" name="description" class="form-control"><?php $this->ee($input->description); ?></textarea>
          <?php $this->showError('description'); ?>
        </div>

        <div class="form-group">
          <button class="btn btn-primary">
            <span class="glyphicon glyphicon-ok"></span>
            <?= $verb ?>
          </button>

          <?php if ($action === "update"): ?>
            <a href="./?page=beverages.delete&amp;id=<?= $input->id ?>" class="btn btn-link" id="delete">
              <span class="glyphicon glyphicon-trash"></span>
              Delete
            </a>
          <?php endif; ?>
        </div>

      </form>
    </div>
  </div>
</div>
