<div class="container">
  <div class="row">
    <div class="col-md-12">
      <h1>Locations</h1>
      <ul>
        <?php foreach ($locations as $location): ?>
          <li><a href="./?page=locations.show&amp;id=<?= $location->id ?>"><?= $location->name ?></a></li>
        <?php endforeach; ?>
      </ul>
      <a class="btn btn-default" href="./?page=locations.create"><span class="glyphicon glyphicon-plus"></span> Add</a>
    </div>
  </div>
</div>
