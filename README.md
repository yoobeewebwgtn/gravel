# README #

Gravel is supposed to be a fully-featured, but very easy to follow MVC framework, written in PHP. It's inspired by Laravel.

### How do I get set up? ###

* Extract to a web folder.
* Change config.inc.php to use proper database connection details
* Import gravel.sql to your database
* profit!

### Built-in Users ###
* alice@alice.com - alice - admin
* bob@bob.com - bob - user

### Who do I talk to? ###

* Repo owner or admin