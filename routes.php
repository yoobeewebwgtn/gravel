<?php namespace Gravel;

$page = isset($_GET['page']) ? $_GET['page'] : 'home';

switch ($page) {
    case "home":
        $controller = new PageController();
        $controller->home();
        break;

//	case "about":
//		$controller = new PageController();
//		$controller->about();
//		break;


    case "auth.login":
        $controller = new AuthenticationController();
        $controller->login();
        break;

    case "auth.attempt":
        $controller = new AuthenticationController();
        $controller->attempt($_POST);
        break;

    case "auth.logout":
        $controller = new AuthenticationController();
        $controller->logout();
        break;

    case "auth.create":
        $controller = new AuthenticationController();
        $controller->create();
        break;

    case "auth.store":
        $controller = new AuthenticationController();
        $controller->store($_POST);
        break;

// contact

    case "contact":
        $controller = new ContactFormController();
        $controller->form();
        break;

    case "contact.submit":
        $controller = new ContactFormController();
        $controller->submit($_POST);
        break;

    case "contact.success":
        $controller = new ContactFormController();
        $controller->success();
        break;

// locations

    case "locations":
        $controller = new LocationController();
        $controller->index();
        break;

    case "locations.create":
        $controller = new LocationController();
        $controller->create();
        break;

    case "locations.store":
        $controller = new LocationController();
        $controller->store($_POST);
        break;

    case "locations.show":
        $controller = new LocationController();
        $controller->show($_GET['id']);
        break;

    case "locations.edit":
        $controller = new LocationController();
        $controller->edit($_GET['id']);
        break;

    case "locations.update":
        $controller = new LocationController();
        $controller->update($_POST);
        break;

    case "locations.delete":
        $controller = new LocationController();
        $controller->delete($_GET['id']);
        break;

    case "locations.destroy":
        $controller = new LocationController();
        $controller->destroy($_POST);
        break;

// beverages

    case "beverages":
        $controller = new BeverageController();
        $controller->index();
        break;

    case "beverages.create":
        $controller = new BeverageController();
        $controller->create();
        break;

    case "beverages.store":
        $controller = new BeverageController();
        $controller->store($_POST);
        break;

    case "beverages.show":
        $controller = new BeverageController();
        $controller->show($_GET['id']);
        break;

    case "beverages.edit":
        $controller = new BeverageController();
        $controller->edit($_GET['id']);
        break;

    case "beverages.update":
        $controller = new BeverageController();
        $controller->update($_POST);
        break;

    case "beverages.delete":
        $controller = new BeverageController();
        $controller->delete($_GET['id']);
        break;

    case "beverages.destroy":
        $controller = new BeverageController();
        $controller->destroy($_POST);
        break;

// beverage categories

    case "beveragecategories":
        $controller = new BeverageCategoryController();
        $controller->index();
        break;

    case "beveragecategories.create":
        $controller = new BeverageCategoryController();
        $controller->create();
        break;

    case "beveragecategories.store":
        $controller = new BeverageCategoryController();
        $controller->store($_POST);
        break;

    case "beveragecategories.show":
        $controller = new BeverageCategoryController();
        $controller->show($_GET['id']);
        break;

    case "beveragecategories.edit":
        $controller = new BeverageCategoryController();
        $controller->edit($_GET['id']);
        break;

    case "beveragecategories.update":
        $controller = new BeverageCategoryController();
        $controller->update($_POST);
        break;

    case "beveragecategories.delete":
        $controller = new BeverageCategoryController();
        $controller->delete($_GET['id']);
        break;

    case "beveragecategories.destroy":
        $controller = new BeverageCategoryController();
        $controller->destroy($_POST);
        break;

    default:
        throw new ModelNotFoundException();
}
