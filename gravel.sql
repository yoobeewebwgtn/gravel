-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 11, 2014 at 03:13 am
-- Server version: 5.5.36
-- PHP Version: 5.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gravel`
--

-- --------------------------------------------------------

--
-- Table structure for table `beverages`
--

CREATE TABLE IF NOT EXISTS `beverages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `category_id` bigint(10) unsigned NOT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `beverages`
--

INSERT INTO `beverages` (`id`, `name`, `description`, `category_id`) VALUES
(5, 'Coca Cola', 'This is it!', 2),
(6, 'Fanta', 'Orange Soda', 2),
(7, 'White Russian', 'Dude…', 3),
(8, 'Black Russian', 'All glory to the motherland!', 3),
(11, 'Espresso', 'Make it quick!', 1),
(12, 'Cappuchino', 'Put some foam on it.', 1),
(13, 'L&P', 'Lemon and Paeroa', 2),
(14, 'Sprite', 'Lemonade', 2),
(19, 'Chicken', '', 5);

-- --------------------------------------------------------

--
-- Table structure for table `beverage_categories`
--

CREATE TABLE IF NOT EXISTS `beverage_categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `beverage_categories`
--

INSERT INTO `beverage_categories` (`id`, `name`) VALUES
(1, 'Coffee'),
(2, 'Cold Drinks'),
(3, 'Alcoholic Drinks'),
(5, 'Soup');

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE IF NOT EXISTS `locations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `description` text NOT NULL,
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `slug`, `name`, `city`, `description`) VALUES
(1, 'mission-bay', 'Mission Bay', 'Auckland', 'A place to sit and look at the water!'),
(2, 'mt-victoria', 'Mt. Victoria', 'Wellington', 'A great lookout spot. Wear a windbreaker.'),
(3, 'port-hills', 'Port Hills', 'Christchurch', 'A nice place to look down on the port. Ride a bike down!');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(20) NOT NULL DEFAULT 'user',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `role`) VALUES
(1, 'alice@alice.com', '$2y$10$n6eVPU7YugzCvnDv06I25eNAMFiurN4Iaajq2BtZ6ni2.sWEaZ4UO', 'admin'),
(2, 'bob@bob.com', '$2y$10$Ahwh0UiB8HsF.Gd2hvdhMufTi7LEKjQseeWmo.ktwVNe8M.xVzJee', 'user');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `beverages`
--
ALTER TABLE `beverages`
  ADD CONSTRAINT `beverages_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `beverage_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
